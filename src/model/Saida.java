/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author alandi
 */
public class Saida {
    private int codigo;
    private String nome;
    private int valor;

    public Saida(int codigo, String nome, int valor) {
        this.codigo = codigo;
        this.nome = nome;
        this.valor = valor;
    }

    public Saida() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
    
}
