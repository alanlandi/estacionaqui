/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author alandi
 */
public class Cliente extends Pessoa{
    private int codigoCliente;

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public boolean isMensalista() {
        return mensalista;
    }

    public void setMensalista(boolean mensalista) {
        this.mensalista = mensalista;
    }

    public int getIdVeiculo() {
        return idVeiculo;
    }

    public void setIdVeiculo(int idVeiculo) {
        this.idVeiculo = idVeiculo;
    }

    public int getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }

    public Cliente(int codigoCliente, boolean mensalista, int idVeiculo, int idPessoa, int codigo, String nome, int cpf, int telefone, String endereco) {
        super(codigo, nome, cpf, telefone, endereco);
        this.codigoCliente = codigoCliente;
        this.mensalista = mensalista;
        this.idVeiculo = idVeiculo;
        this.idPessoa = idPessoa;
    }

    public Cliente(int codigoCliente, boolean mensalista, int idVeiculo, int idPessoa) {
        this.codigoCliente = codigoCliente;
        this.mensalista = mensalista;
        this.idVeiculo = idVeiculo;
        this.idPessoa = idPessoa;
    }
    private boolean mensalista;
    private int idVeiculo;
    private int idPessoa;

    public Cliente() {
    }

}